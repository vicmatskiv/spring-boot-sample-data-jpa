# spring-boot-sample-data-jpa

This is a demo app including the following features

- Spring Data JPA
- Apache OpenJPA (instead of Hibernate)
- *No* Hibernate dependencies (other than hibernate-validator)
- Uses H2 db to run the app or unit tests locally
- Cloud deployment manifest
- Binds to Postgres db when runs in the cloud
- Liquibase initialization
- Web Controller

### How to build the project

The project can be built with maven as follows:

```sh
mvn clean install -s settings.xml
```

### How to configure the project

#### Changing local configuration

Changes to the local configuration should be made via _config/application.properties_.
Never ever put any local (or environment specific configuration) under _src/main/resources_ directory.

#### Changing cloud configuration

Changes to the cloud configuration should be made via _manifest.yml_.

#### Changing common configuration.

Changes applicable to *both* local and cloud configuration can be done via _src/main/resources/application.properties_.


### How to run the app 

#### Running locally

To run the project locally run the following command from the current project directory:

```sh
java -jar target/spring-boot-sample-data-jpa-1.5.9.RELEASE.jar
```
#### Access H2 console

H2 console is enabled when the app runs locally and can be accessed with a browser at the following URL:

http://localhost:8080/h2-console

The console credentials can be found in config/application.properties.

The app exposes web endpoint that can be accessed using the following URL

http://localhost:8080

#### Running in the cloud

To deploy the app, from the current project directory, run the following command:

```sh
cf push
```

The app exposes web endpoint that can be accessed using the following URL:

https://tmp-sd001.run.aws-usw02-dev.ice.predix.io
